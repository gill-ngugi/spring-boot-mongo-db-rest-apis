# INSTALL MONGODB LOCALLY

To install MongoDB with homebrew
----------------------------------
https://stackoverflow.com/questions/57856809/installing-mongodb-with-homebrew
https://treehouse.github.io/installation-guides/mac/mongo-mac.html#:~:text=Run%20the%20Mongo%20daemon%2C%20in%20one%20terminal%20window%20run%20~%2F,to%20access%20data%20in%20MongoDB.

To install Mongo Compass
---------------------------
https://www.mongodb.com/try/download/compass?tck=docs_compass

To run
---------
0. Make sure application.properties is set up.

1. Open two terminals

2. https://stackoverflow.com/questions/13827915/location-of-the-mongodb-database-on-mac#:~:text=The%20databases%20are%20stored%20in,%2Flocal%2Fetc%2Fmongod. 
	# On the first terminal 👇🏼👇🏼
	-----------------------------
	To run it (if you installed it with homebrew) run the process like this:
	=>> mongod --dbpath /usr/local/var/mongodb

3.  # Mongo Compass 
    ------------------
    Then launch Mongo Compass from applications
    =>> Then click on create new connection.

4. # On the second terminal 👇🏼👇🏼
   -----------------------------
   <-- This successfully connects you to the database - Mongo Shell -->
   Just type in second terminal: mongo 
   
5. Show databases; OR Show dbs;
   # Create a DB that will be referenced in applications.properties
       use gillCode;
       db.getName(); ...Displays name of created/referenced db; -> Returns gillNewDb;
       db.createCollection("person");
       show collections;
       db.person.stats();
       db.person.drop();
       student = {"id": "1", "name": "Damon"}
       db.person.insert(student);
           OR
           db.person.insert({});
       db.person.count(); //Will return 1
       show collections; //Will return student because it has been added.
       db.student.find(); // Returns the student object.
       db.student.find().pretty(); //Returns the student object prettified.
      
6. # Run application in main class.
      
7. # Execute a request on Postman 

 
   



