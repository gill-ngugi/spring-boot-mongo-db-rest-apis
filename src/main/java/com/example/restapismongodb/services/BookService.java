package com.example.restapismongodb.services;

import com.example.restapismongodb.models.Book;
import com.example.restapismongodb.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    // Post one book
    public Book saveBook(Book book) {
        return bookRepository.save(book);
    }

    // Post many books
    public List<Book> saveBooks(List<Book> books) {
        return bookRepository.saveAll(books);
    }

    // Get many books
    public List<Book> getBooks() {
        return bookRepository.findAll();
    }

    // Get a book by id
    public Book getBookById(String id) {
        return bookRepository.findById(id).orElse(null);
    }

    // Get a book by bookName
    public Book getBookByName(String bookName) {
        return bookRepository.findBookByBookName(bookName).orElse(null);
    }

    // Delete a book by id
    public String deleteBookById(String id) {
        bookRepository.deleteById(id);
        return "Book Deleted: " + id;
    }

    // Update a book
    public Book updateBook(Book book) {
        Book existingBook = bookRepository.findById(book.getId()).orElse(null);
        existingBook.setBookName(book.getBookName());
        existingBook.setAuthorName(book.getAuthorName());
        return bookRepository.save(existingBook);
    }
}
