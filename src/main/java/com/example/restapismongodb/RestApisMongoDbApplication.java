package com.example.restapismongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class RestApisMongoDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApisMongoDbApplication.class, args);
	}

}
