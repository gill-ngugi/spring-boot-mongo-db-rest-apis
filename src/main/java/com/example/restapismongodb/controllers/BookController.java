package com.example.restapismongodb.controllers;

import com.example.restapismongodb.models.Book;
import com.example.restapismongodb.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1")
@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    @PostMapping("/book")
    public Book addBook(@RequestBody Book book) {
        return bookService.saveBook(book);
    }

    @PostMapping("/books")
    public List<Book> addBooks(@RequestBody List<Book> books){
        return bookService.saveBooks(books);
    }

    @GetMapping("/books")
    public List<Book> getAllBooks() {
        return bookService.getBooks();
    }

    @GetMapping("/book/{id}")
    public Book getBookById(@PathVariable String id) {
        return bookService.getBookById(id);
    }

    @GetMapping("/bookByName/{bookName}")
    public Book getBookByName(@PathVariable String bookName) {
        return bookService.getBookByName(bookName);
    }

    @DeleteMapping("/book/{id}")
    public String deleteBook(@PathVariable String id) {
        return bookService.deleteBookById(id);
    }

    @PutMapping("/book")
    public Book updateBook(@RequestBody Book book) {
        return bookService.updateBook(book);
    }

}

